FROM rust:1.43.0-slim-buster as base

RUN set -ex \
    && apt-get -yq update \
    && apt-get install -yq --no-upgrade \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    && apt-get -yq clean \ 
    && rm -rf /var/lib/apt/lists/*

FROM base as build

RUN set -ex \
    && apt-get -yq update \
    && apt-get install -yq --no-upgrade \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    libgstrtspserver-1.0-dev libges-1.0-dev \
    && apt-get -yq clean \ 
    && rm -rf /var/lib/apt/lists/*

FROM build as dev

RUN set -ex \
    && apt-get -yq update \
    && apt-get install -yq --no-upgrade \
    gstreamer1.0-tools \
    && apt-get -yq clean \ 
    && rm -rf /var/lib/apt/lists/*



