SHELL := /bin/bash
include .env

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help build shell
.DEFAULT_GOAL := help

.EXPORT_ALL_VARIABLES:
DISPLAY ?=$(shell w $$(id -un) | awk 'NF > 7 && $$2 ~ /tty[0-9]+/ {print $$3; exit}')
SELENIUM_HOST ?=

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

build:
	docker-compose build

shell:
	docker-compose run --rm dev /bin/bash
